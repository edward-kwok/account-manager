# Getting Started

### Tech Stack
1. Spring Boot 2.5.5
2. Guava 31
3. Swagger2
4. lombok
5. JUnit 5


### Build
Linux environment
`./gradlew build`

Windows environment
`gradlew.bat build`

### Run
Linux
`./gradlew run`

Windows
`gradlew.bat run`


### Usage
User can refer to swagger api documentation provided when the application is up
http://localhost:8080/swagger-ui/#/


**Simple usage**

### GET api/v1/accounts/{code}

#### Request

Get the Account entity details

Example
`http://localhost:8080/api/v1/accounts/12345678`

#### Response 

Account entity in body

Status code `200 OK`

**Example**

```{
    "id": 1,
    "balance": 1000000.00,
    "code": "12345678",
    "version": 2
}
```
#### Exception

|   Exception    | Status Code |
| ----------- | ----------- |
| Cannot find account      | `404 NOT FOUND`       |

### PUT api/v1/accounts

#### Request

Body
```
TransactionDto{
    amount*	number
    minimum: 0.01
    exclusiveMinimum: false
    fromCode	string
    id*	string($uuid)
    toCode	string*
}
```
For id, it is assumed client generated a random uuid to fill in the field.


**Example**

`http://localhost:8080/api/v1/accounts`

Request Body input
```
{
    "amount": 1,
    "fromCode": "88888888",
    "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "toCode": "12345678"
}
```

#### Response

##### Success

Return status code`200 OK`


#### Exception

|   Exception    | Status Code |
| ----------- | ----------- |
| Invalid transfer details     | `400 BAD REQUEST` |
| Account from/to not found     | `404 NOT FOUND` |
| Transaction exists in system     | `409 CONFLICT` |
| Account locked during transfer | `503 SYSTEM UNAVAILABLE`|

### Assumption
1. User are not required to login. (ie. Security is not implemented in this api)
2. API is supposed to be running on single JVM. If it is deployed on multi jvm environment,
PUT api would not be impotent, as transaction id uniqueness in maintained in each JVM memory.
3. Rate Limiting - It is supposed that there would be API gateway for throttling/rate limiting for user request.
4. The transfer request is impotent, same transaction id can be sent to the system once. It prevents user triggering 
the request again when the system is not responding.
5. During money transfer, accounts are locked using Optimistic locking. If there is stale data during transfer, 
it would return 503 Service Unavailable, indicated that user can retry with different transaction id.
6. All transactions and balances are in HKD. User cannot choose what to transfer/get.
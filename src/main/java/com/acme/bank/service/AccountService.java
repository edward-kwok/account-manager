package com.acme.bank.service;

import com.acme.bank.dto.TransactionDto;
import com.acme.bank.entity.Account;
import com.acme.bank.exception.AccountNotFoundException;
import com.acme.bank.exception.RepeatedTransactionReqException;
import com.acme.bank.exception.TransactionException;
import com.acme.bank.repository.AccountRepository;
import com.google.common.base.Objects;
import com.google.common.cache.Cache;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private Cache<UUID,TransactionDto> transactionCache;

    //Provide the transaction manager for locking.
    @Transactional(readOnly = true)
    public Account getAccountFromCode(String code) {
       return accountRepository.findByAccountCode(code).orElseThrow(() -> new AccountNotFoundException(code));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void transfer(TransactionDto transactionDto) {
        //Simple validation..
        if (Objects.equal(transactionDto.getFromCode().trim(),transactionDto.getToCode().trim())) {
            throw new TransactionException("From/to are in same account");
        }

        //Check transaction
        if (transactionCache.getIfPresent(transactionDto.getId()) == null) {
            transactionCache.put(transactionDto.getId(),transactionDto);
            log.debug("Transaction not exists before. Adding transaction {} into cache", transactionDto.getId().toString());
        } else {
            throw new RepeatedTransactionReqException(transactionDto.getId());
        }

        Account to = null;
        Account from = null;

        //Accquire lock using the same order..
        if (transactionDto.getFromCode() != null &&
                transactionDto.getToCode()!= null && transactionDto.getFromCode().compareTo(transactionDto.getToCode()) == -1) {
             from = accountRepository.findByAccountCode(transactionDto.getFromCode()).orElseThrow(()->new AccountNotFoundException(transactionDto.getFromCode()));
             to = accountRepository.findByAccountCode(transactionDto.getToCode()).orElseThrow(()-> new AccountNotFoundException(transactionDto.getToCode()));
        } else {
             to = accountRepository.findByAccountCode(transactionDto.getToCode().trim()).orElseThrow(()-> new AccountNotFoundException(transactionDto.getToCode()));
             from = accountRepository.findByAccountCode(transactionDto.getFromCode().trim()).orElseThrow(()->new AccountNotFoundException(transactionDto.getFromCode()));
        }

        if (from.getBalance().compareTo(transactionDto.getAmount()) > 0){
            log.info("From account: {} having balance {}, which is greater than transfer amount {}",transactionDto.getFromCode(),from.getBalance(),transactionDto.getAmount());
            from.setBalance(from.getBalance().subtract(transactionDto.getAmount()));
            to.setBalance(to.getBalance().add(transactionDto.getAmount()));
            accountRepository.saveAllAndFlush(Arrays.asList(from,to));
        } else {
            throw new TransactionException(String.format("Account %s not enough balance for transferring %s",transactionDto.getFromCode(), transactionDto.getAmount()));
        }
        log.info("{} transaction is done, from code: {}, from balance: {}, to code: {}, code balance: {}",transactionDto, from.getCode(), from.getBalance(),to.getCode(),to.getBalance());
    }

}

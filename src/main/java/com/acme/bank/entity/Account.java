package com.acme.bank.entity;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ACCOUNT",  indexes = {@Index(name = "code_idx",  columnList="code", unique = true)})
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "balance", nullable=false)
    private BigDecimal balance;
    @Column(name = "code", nullable=false, unique=true)
    private String code;
    @Version
    private Integer version;
}

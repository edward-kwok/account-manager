package com.acme.bank.dto;


import com.acme.bank.validator.ValidUuid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public final class TransactionDto {
    @NotNull
    @ValidUuid
    private UUID id;
    @NotNull
    @NotEmpty(message = "Please provide from account code")
    private String fromCode;
    @NotNull
    @NotEmpty(message = "Please provide to account code")
    private String toCode;
    @NotNull(message = "Please provide transfer balance")
    @DecimalMin(value = "0.01", message = "Amount should be larger than 0.01")
    private BigDecimal amount;

}

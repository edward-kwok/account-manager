package com.acme.bank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(value= HttpStatus.CONFLICT)
public class RepeatedTransactionReqException extends TransactionException{
    public RepeatedTransactionReqException(UUID id) {
        super(String.format("Transaction %s already exists", id.toString()));
    }
}

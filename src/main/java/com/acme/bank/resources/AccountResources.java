package com.acme.bank.resources;


import com.acme.bank.dto.TransactionDto;
import com.acme.bank.entity.Account;
import com.acme.bank.service.AccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api")
public class AccountResources {
    @Autowired
    private AccountService accountService;

    @ApiOperation("Get account details with balance")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Account successfully retrieved",
                    content = {@Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = Account.class))}),
            @ApiResponse(responseCode=  "404", description=  "Account not found"),
    })
    @GetMapping(value = "v1/accounts/{code}", produces = APPLICATION_JSON_VALUE)
    Account one(@PathVariable String code) {
        return accountService.getAccountFromCode(code);
    }

    @ApiOperation("Transfer money from 1 account to another")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Updated specified from/to Account balances"),
            @ApiResponse(responseCode=  "400", description= "Wrong transaction details in body"),
            @ApiResponse(responseCode=  "404", description= "Cannot find from/to account"),
            @ApiResponse(responseCode=  "409", description= "Transaction exists, please use another transaction id for new transaction"),
            @ApiResponse(responseCode=  "503", description= "Account transfer in progress, user can try again later")
    })
    @PutMapping(value = "v1/accounts",consumes = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    void transaction(@Valid @RequestBody TransactionDto transactionDto) {
        accountService.transfer(transactionDto);
    }

}

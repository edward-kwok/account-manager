package com.acme.bank.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class UuidValidator implements ConstraintValidator<ValidUuid, UUID> {
    private static final String regex = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

    @Override
    public void initialize(ValidUuid validUuid) { }


    @Override
    public boolean isValid(UUID uuid, ConstraintValidatorContext cxt) {
        if (uuid == null) {
            return false;
        }

        return uuid.toString().matches(regex);
    }
}

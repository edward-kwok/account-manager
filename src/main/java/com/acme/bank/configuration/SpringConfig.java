package com.acme.bank.configuration;

import com.acme.bank.dto.TransactionDto;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.Duration;
import java.util.UUID;

@Configuration
@EnableSwagger2
@Slf4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SpringConfig {

    @Bean
    public Cache<UUID, TransactionDto> transactionDtoMap() {
        RemovalListener<UUID, TransactionDto> listener = n -> {
            if (n.wasEvicted()) {
                String cause = n.getCause().name();
                log.debug("Transaction {} is evicted from map, transaction id can be reused. Cause: {}", n, cause);
            }
        };
        return CacheBuilder.newBuilder().expireAfterWrite(Duration.ofMinutes(60)).removalListener(listener).build();
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}

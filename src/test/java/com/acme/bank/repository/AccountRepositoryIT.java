package com.acme.bank.repository;

import com.acme.bank.entity.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@ComponentScan
public class AccountRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountRepository accountRepository;

    private static final String account1 = "12345678";
    private static final String account2 = "88888888";
    private static final Account fromAccount = new Account(1L,new BigDecimal("10000"),account1,1);
    private static final Account toAccount = new Account(2L,new BigDecimal("10000"),account2,1);

    @Test
    public void testSaveAndFindAccount() {
        accountRepository.saveAndFlush(fromAccount);
        Optional<Account> fromDbAccount = accountRepository.findByAccountCode(account1);
        if (fromDbAccount.isPresent()) {
            Account fromDb = fromDbAccount.get();
            assertEquals(fromAccount.getId(),fromDb.getId());
            assertEquals(fromAccount.getBalance(),fromDb.getBalance());
            assertEquals(fromAccount.getCode(), fromDb.getCode());
        }
    }
}

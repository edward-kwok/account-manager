package com.acme.bank.validator;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UuidValidatorTest {

    private final static UuidValidator uuidValidator = new UuidValidator();

    @Test
    public void testValid() {
        assertTrue(uuidValidator.isValid(UUID.randomUUID(), Mockito.mock(ConstraintValidatorContext.class)));
    }



}

package com.acme.bank.service;

import com.acme.bank.dto.TransactionDto;
import com.acme.bank.entity.Account;
import com.acme.bank.exception.AccountNotFoundException;
import com.acme.bank.exception.RepeatedTransactionReqException;
import com.acme.bank.exception.TransactionException;
import com.acme.bank.repository.AccountRepository;
import com.google.common.cache.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Optional;
import java.util.UUID;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceTest {

    private AccountRepository accountRepository;

    private Cache<UUID, TransactionDto> map;

    private AccountService accountService;

    private final String account1 = "12345678";
    private final String account2 = "88888888";
    private final Account accountFrom = new Account(1L,new BigDecimal("10000000"),account1,1);
    private final Account accountTo = new Account(1L,new BigDecimal("10000000"), account2,1);

    @BeforeEach
    public void all() {
        accountRepository = mock(AccountRepository.class);
        //Using 1s to test, actual implementation is 60 mins
        map = CacheBuilder.newBuilder().expireAfterWrite(Duration.ofSeconds(1)).build();
        accountService = new AccountService(accountRepository, map);
    }



    @Test
    public void testRetrievingExistingAccount() {
        when(accountRepository.findByAccountCode(account1)).thenReturn(Optional.of(accountFrom));

        Account retrieved = accountService.getAccountFromCode(account1);
        assertEquals(accountFrom,retrieved);
    }

    @Test
    public void testRetrievingNonExistingAccount() {
        String wrongCode = account1;
        Account account = new Account(1L,new BigDecimal("10000000"),"1",1);
        when(accountRepository.findByAccountCode(wrongCode)).thenThrow(new AccountNotFoundException(wrongCode));

        Exception exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.getAccountFromCode(wrongCode);
        });

        String expectedMessage = "Could not find account " + wrongCode;
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testTransferToNonExistenceAccount() {
        String wrongCode = "1";
        when(accountRepository.findByAccountCode(wrongCode)).thenThrow(new AccountNotFoundException(wrongCode));
        when(accountRepository.findByAccountCode(account1)).thenReturn(Optional.of(accountFrom));
        TransactionDto transferDto = new TransactionDto(UUID.randomUUID(),account1,wrongCode,new BigDecimal(2));

        Exception exception = assertThrows(AccountNotFoundException.class, () -> {
            accountService.getAccountFromCode(wrongCode);
        });

        String expectedMessage = "Could not find account " + wrongCode;
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testTransactionExist() {
        when(accountRepository.findByAccountCode(account1)).thenReturn(Optional.of(accountFrom));
        when(accountRepository.findByAccountCode(account2)).thenReturn(Optional.of(accountTo));

        TransactionDto transactionDto = new TransactionDto(UUID.randomUUID(),account1,account2,new BigDecimal("2"));

        accountService.transfer(transactionDto);
        //Retrying
        Exception exception = assertThrows(RepeatedTransactionReqException.class, () -> {
            accountService.transfer(transactionDto);
        });

        String expectedMessage = String.format("Transaction %s already exists",transactionDto.getId().toString());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void testTransferNotEnoughBalance() {
        when(accountRepository.findByAccountCode(account1)).thenReturn(Optional.of(accountFrom));
        when(accountRepository.findByAccountCode(account2)).thenReturn(Optional.of(accountTo));

        TransactionDto dto = new TransactionDto(UUID.randomUUID(),account1,account2,new BigDecimal("10000001"));

        Exception exception = assertThrows(TransactionException.class, () -> {
            accountService.transfer(dto);
        });

        String expectedMessage = String.format("Account %s not enough balance for transferring %s", dto.getFromCode(), dto.getAmount());
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testTransactionExpiry() throws InterruptedException {
        when(accountRepository.findByAccountCode(account1)).thenReturn(Optional.of(accountFrom));
        when(accountRepository.findByAccountCode(account2)).thenReturn(Optional.of(accountTo));

        TransactionDto dto = new TransactionDto(UUID.randomUUID(),account1,account2,new BigDecimal("1"));
        accountService.transfer(dto);

        Thread.sleep(1001);
        //Allowing to create another transaction
        accountService.transfer(dto);
    }
}

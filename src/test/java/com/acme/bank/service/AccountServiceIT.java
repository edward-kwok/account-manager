package com.acme.bank.service;

import com.acme.bank.dto.TransactionDto;
import com.acme.bank.entity.Account;
import com.acme.bank.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class AccountServiceIT {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @PersistenceContext
    private EntityManager entityManager;

    private static final String account1 = "12345678";
    private static final String account2 = "88888888";

    private static final Account fromAccount = new Account(1L, new BigDecimal("10000"), account1, 1);
    private static final Account toAccount = new Account(2L, new BigDecimal("10000"), account2, 1);



    //This test testing for Optimistic Locking..
    @BeforeEach
    public void clearEntities() {
        accountRepository.deleteAll();
        accountRepository.flush();
    }

    @Test
    public void testDirtyRead() throws InterruptedException {
        accountRepository.saveAllAndFlush(Arrays.asList(fromAccount, toAccount));
        entityManager.detach(fromAccount);
        entityManager.detach(toAccount);
        //Two accounts
        final ExecutorService executor = Executors.newFixedThreadPool(2);

        List<TransactionDto> dtos = Arrays.asList(new TransactionDto(UUID.randomUUID(), fromAccount.getCode(), toAccount.getCode(), new BigDecimal("10")),
                new TransactionDto(UUID.randomUUID(), fromAccount.getCode(), toAccount.getCode(), new BigDecimal(20)));
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Got Optimistic Locking failure");
                assertTrue(e instanceof ObjectOptimisticLockingFailureException);
            }
        });

        for (final TransactionDto dto : dtos) {
            executor.execute(() -> accountService.transfer(dto));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);

    }

    @Test
    public void testDeadLock() throws InterruptedException {
        accountRepository.saveAllAndFlush(Arrays.asList(fromAccount, toAccount));
        entityManager.detach(fromAccount);
        entityManager.detach(toAccount);
        //Two accounts
        final ExecutorService executor = Executors.newFixedThreadPool(2);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Got Optimistic Locking failure");
                assertTrue(e instanceof ObjectOptimisticLockingFailureException);
            }
        });

        List<TransactionDto> dtos = Arrays.asList(new TransactionDto(UUID.randomUUID(), fromAccount.getCode(), toAccount.getCode(), new BigDecimal("10")),
                new TransactionDto(UUID.randomUUID(), toAccount.getCode(), fromAccount.getCode(), new BigDecimal(20)));

        for (final TransactionDto dto : dtos) {
            executor.execute(() -> accountService.transfer(dto));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }
}

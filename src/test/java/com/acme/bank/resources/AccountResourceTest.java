package com.acme.bank.resources;

import com.acme.bank.dto.TransactionDto;
import com.acme.bank.entity.Account;
import com.acme.bank.exception.AccountNotFoundException;
import com.acme.bank.exception.RepeatedTransactionReqException;
import com.acme.bank.exception.TransactionException;
import com.acme.bank.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountResources.class)
public class AccountResourceTest {

    @MockBean
    private AccountService accountService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static final String ACCOUNT_CODE_FROM = "12345678";

    private static final String ACCOUNT_CODE_TO = "88888888";

    private static final Account ACCOUNT_FROM = new Account(1L,new BigDecimal("10000000"),ACCOUNT_CODE_FROM, 1);

    @Test
    public void getAccountSuccessfully_return200() throws Exception {
        Mockito.when(accountService.getAccountFromCode(ACCOUNT_CODE_FROM)).thenReturn(ACCOUNT_FROM);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/accounts/12345678")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(objectMapper.writeValueAsString(ACCOUNT_FROM)));
    }

    @Test
    public void transferFailure_return400() throws Exception {
        TransactionDto dto = new TransactionDto(UUID.randomUUID(),ACCOUNT_CODE_FROM, ACCOUNT_CODE_TO,new BigDecimal("1") );
        Mockito.doThrow(new TransactionException(String.format("Account %s not enough balance for transferring %s",dto.getFromCode(), dto.getAmount()))).when(accountService).transfer(dto);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1/accounts")
                        .content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void failedToGetAccount_return404() throws Exception {
        String wrongCode = "1";
        Mockito.when(accountService.getAccountFromCode(wrongCode)).thenThrow(new AccountNotFoundException("Could not find account " + wrongCode));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/accounts/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testInvalidTransactionDto_return400() throws Exception {
        TransactionDto dto1 = new TransactionDto(null,null,"12345678",new BigDecimal("0"));
        TransactionDto dto2 = new TransactionDto(UUID.randomUUID(),null,"12345678",new BigDecimal("2"));
        TransactionDto dto3 = new TransactionDto(UUID.randomUUID(),"88888888","12345678",new BigDecimal("0"));
        TransactionDto dto4 = new TransactionDto(UUID.randomUUID(),"88888888",null,new BigDecimal("0"));

        List<TransactionDto> dtos = List.of(dto1,dto2,dto3,dto4);

        for(TransactionDto t : dtos) {

            mockMvc.perform(MockMvcRequestBuilders
                            .put("/api/v1/accounts")
                            .content(objectMapper.writeValueAsString(t)).contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andExpect(status().isBadRequest());
        }

    }

    @Test
    public void transferSuccessful_return200() throws Exception {
        TransactionDto dto = new TransactionDto(UUID.randomUUID(),ACCOUNT_CODE_FROM, ACCOUNT_CODE_TO,new BigDecimal("1") );
        Mockito.doNothing().when(accountService).transfer(dto);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1/accounts")
                        .content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON_VALUE))
                        .andExpect(status().isOk());
    }

    @Test
    public void repeatedTransaction_return409() throws Exception {
        TransactionDto dto = new TransactionDto(UUID.randomUUID(),ACCOUNT_CODE_FROM, ACCOUNT_CODE_TO,new BigDecimal("1") );
        Mockito.doThrow(new RepeatedTransactionReqException(dto.getId())).when(accountService).transfer(dto);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1/accounts")
                        .content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isConflict());
    }

    @Test
    public void sameFromToAccountInTransaction_return400() throws Exception {
        TransactionDto dto = new TransactionDto(UUID.randomUUID(),ACCOUNT_CODE_FROM, ACCOUNT_CODE_FROM,new BigDecimal("1") );
        Mockito.doThrow(new TransactionException("From/to are in same account")).when(accountService).transfer(dto);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1/accounts")
                        .content(objectMapper.writeValueAsString(dto)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testInvalidUUID_return400() throws Exception {
        String json = "{   \n" +
                "    \"id\": \"1\",\n" +
                "    \"fromCode\": \"88888888\",\n" +
                "    \"toCode\": \"12345678\",\n" +
                "    \"amount\": 1\n" +
                "}";
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1/accounts")
                        .content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());

    }




}
